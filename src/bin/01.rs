use std::{convert::Infallible, str::FromStr};

use advent_of_code::helpers::ReadHelper;
use itertools::Itertools;

pub struct Calories(Vec<u32>);

impl FromStr for Calories {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(s.as_bytes().split_lines()))
    }
}

impl Calories {
    fn sum(&self) -> u32 {
        self.0.iter().sum()
    }
}

pub fn part_one(input: &str) -> Option<u32> {
    let groups: Vec<Calories> = input.as_bytes().split_groups();
    groups.iter().map(|v| v.sum()).max()
}

pub fn part_two(input: &str) -> Option<u32> {
    let groups: Vec<Calories> = input.as_bytes().split_groups();
    groups
        .iter()
        .map(|v| v.sum())
        .sorted_unstable_by(|a, b| b.cmp(a))
        .take(3)
        .sum1()
}

fn main() {
    let input = &advent_of_code::read_file("inputs", 1);
    advent_of_code::solve!(1, part_one, input);
    advent_of_code::solve!(2, part_two, input);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = advent_of_code::read_file("examples", 1);
        assert_eq!(part_one(&input), None);
    }

    #[test]
    fn test_part_two() {
        let input = advent_of_code::read_file("examples", 1);
        assert_eq!(part_two(&input), None);
    }
}
