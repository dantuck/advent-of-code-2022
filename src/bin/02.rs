pub fn part_one(input: &str) -> Option<u32> {
    let score = input
        .lines()
        .into_iter()
        .map(|round| score_round(round.split(' ').collect()))
        .fold(0, |acc, score| acc + score);

    Some(score)
}

pub fn part_two(input: &str) -> Option<u32> {
    let score = input
        .lines()
        .into_iter()
        .map(|round| {
            let round: Vec<&str> = round.split(' ').collect();
            let round = match round[1] {
                // lose
                "X" => match round[0] {
                    "A" => vec!["A", "Z"],
                    "B" => vec!["B", "X"],
                    "C" => vec!["C", "Y"],
                    _ => vec!["", ""],
                },
                // draw
                "Y" => match round[0] {
                    "A" => vec!["A", "X"],
                    "B" => vec!["B", "Y"],
                    "C" => vec!["C", "Z"],
                    _ => vec!["", ""],
                },
                // win
                "Z" => match round[0] {
                    "A" => vec!["A", "Y"],
                    "B" => vec!["B", "Z"],
                    "C" => vec!["C", "X"],
                    _ => vec!["", ""],
                },
                _ => vec!["", ""],
            };
            score_round(round)
        })
        .fold(0, |acc, score| acc + score);

    Some(score)
}

fn score_round(round: Vec<&str>) -> u32 {
    match round[0] {
        // Rock
        "A" => match round[1] {
            "X" => 3 + 1,
            "Y" => 6 + 2,
            "Z" => 0 + 3,
            _ => 0,
        },
        // Paper
        "B" => match round[1] {
            "X" => 0 + 1,
            "Y" => 3 + 2,
            "Z" => 6 + 3,
            _ => 0,
        },
        // Scissors
        "C" => match round[1] {
            "X" => 6 + 1,
            "Y" => 0 + 2,
            "Z" => 3 + 3,
            _ => 0,
        },
        _ => 0,
    }
}

fn main() {
    let input = &advent_of_code::read_file("inputs", 2);
    advent_of_code::solve!(1, part_one, input);
    advent_of_code::solve!(2, part_two, input);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = advent_of_code::read_file("examples", 2);
        assert_eq!(part_one(&input), None);
    }

    #[test]
    fn test_part_two() {
        let input = advent_of_code::read_file("examples", 2);
        assert_eq!(part_two(&input), None);
    }
}
