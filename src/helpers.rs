/*
 * Use this file if you want to extract helpers from your solutions.
 * Example import from this file: `use advent_of_code::helpers::example_fn;`.
 */

use std::{
    io::{BufRead, BufReader, Read},
    str::FromStr,
};

pub trait ReadHelper<T> {
    fn split_lines(self) -> Vec<T>;
    fn split_groups(self) -> Vec<T>;
}

impl<R, T> ReadHelper<T> for R
where
    R: Read,
    T: FromStr,
{
    fn split_lines(self) -> Vec<T> {
        BufReader::new(self)
            .lines()
            .flatten()
            .flat_map(|l| l.parse())
            .collect()
    }

    fn split_groups(self) -> Vec<T> {
        BufReader::new(self)
            .lines()
            .flatten()
            .collect::<Vec<_>>()
            .split(|l| l.is_empty())
            .flat_map(|e| e.join("\n").parse())
            .collect::<Vec<T>>()
    }
}
